# docker-schism

Dockerfiles to compile and run SCHISM.

*SCHISM
Semi-implicit Cross-scale Hydroscience Integrated System Model
Build instructions may be found on the SCHISM wiki at schism.wiki
When using the development version, note changes in flags and features described in
beta_notes and examplified in param.in.sample, bctides.in.sample, etc.*

The `Dockerfiles` are in the `dockerfiles` directory

# Docker Compile SCHISM

Dockerfile `Dockerfile-build` is used to compile SCHISM.

    docker build -t schism-build -f Dockerfile-build .

Run the docker image so one can copy the application/executables to be
used in the runtime docker next:

    docker run -it --name schism schism-build /bin/bash

In another terminal

    docker cp schism:/home/schism_vl .
    docker cp schism:/home/schism_vl_ice .
    docker rm schism

# Docker build runtime SCHISM

To build the docker image with SCHISM executable:

    docker build -t schism-5.4.0 -f Dockerfile-run .

To tag and push to Docker Hub:

    docker login
    docker tag schism-5.4.0 lipcomputing/schism:5.4.0
    docker push lipcomputing/schism:5.4.0

The image can be found in: https://cloud.docker.com/u/lipcomputing/repository/docker/lipcomputing/schism

# Acknowlegments

* Zhang, Y. and Baptista, A.M. (2008)
  SELFE: A semi-implicit Eulerian-Lagrangian finite-element model for
  cross-scale ocean circulation", Ocean Modelling, 21(3-4), 71-96.
* Zhang, Y., Ye, F., Stanev, E.V., Grashorn, S. (2016)
  Seamless cross-scale modeling with SCHISM, Ocean Modelling, 102, 64-81.

SCHISM Manual: http://ccrm.vims.edu/schismweb/SCHISM_v5.6-Manual.pdf

SCHISM:
* http://columbia.vims.edu/schism/
* http://ccrm.vims.edu/schismweb/
